import vue from 'vue'
import maskComponent from './mask.vue'

function showMask(text,showing) {
  // 返回一个 扩展实例构造器
  const MaskConstructor = vue.extend(maskComponent)
  // 实例化一个 mask.vue
  const maskDom = new MaskConstructor({
    el: document.createElement('div'),
    data() {
      return {
        text:text,
        isShow:showing,
      }
    },
  })
  // 把 实例化的mask.vue 添加到 body 里
  document.body.appendChild(maskDom.$el)
}

const registryMask = function() {
  // 将组件注册到 vue 的原型链里去,这样就可以在所有 vue 的实例里面使用 this.$mask()调用组件
  vue.prototype.$mask = showMask
}

export default registryMask