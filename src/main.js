import Vue from 'vue'
import App from './App.vue'
import {router} from './router'
import store from './store'
import axios from 'axios'

// ===============全局js文件=======================
// import Config from '@/config.js'   //全局变量
// Vue.prototype.Config = Config            
import * as Base from './common/base.js'   // 公共js函数
Vue.prototype.Base = Base                   
import vuexData from './common/vuexData.js' 
Vue.mixin(vuexData)  //全局mixin

// ===============全局js文件=======================
import toastRegistry from './components/base/toast'
//import maskRegistry from './components/base/mask'
Vue.use(toastRegistry)//.use(maskRegistry)

// ===============阿里iconfont=======================
import './assets/iconfont/iconfont.js'
import './assets/iconfont/icon.css'

import 'font-awesome/css/font-awesome.css'

import '@/components/_globals'
// ===============第3方ui库 vant=======================
import {Button,cell,CellGroup,Toast,Field,Search} from 'vant'
import '@/assets/styles/vantCustom.scss'
Vue.use(Button).use(cell).use(CellGroup).use(Toast).use(Field).use(Search)

Vue.prototype.$myVue = new Vue; 

Vue.config.productionTip = false

//创建一个拥有通用配置的axios实例
Vue.prototype.axios = axios.create({
  // baseURL: 'https://some-domain.com/api/', //当前项目需要动态设置服务器url，所以不写死
  timeout: 9000,  // 超时
  // headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
})

new Vue({
  router,  //把router实例注入到 vue 根实例,从而可以在任何组件内通过 this.$router 访问路由器
  store,
  render: h => h(App)
}).$mount('#appStart')