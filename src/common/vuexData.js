import { mapMutations, mapGetters,mapActions } from 'vuex'
const vuexData = {
    computed: {
        //获取vuex-state的简化写法
        ...mapGetters({
            $_setupInfo:'setupInfo',
            $_tenderObj:'tenderObj',
            $_userInfo:'userInfo',
            $_goodsArray:'goodsArray',
            $_payTender:'payTender',
            $_cartStore:'cartStore',
            $_salesTotal:'salesTotal',
            $_salesTender:'salesTender'
        })
    },

    methods: {
        ...mapMutations({
            $_setConfiguration: "SETUPINFO_F",
            $_setUserInfo: "USERINFO_F",
            $_setGoodsArray: 'GOODS_ARRAY_F',
            $_setPayTender: 'PAY_TENDER_F',
            $_addCart: 'ADD_CART_F',
            $_updateCartAdd:'UPDATE_CART_ADD_F',
            $_updateCartReduction:'UPDATE_CART_REDUCTION_F',
            $_toPayData:'TO_PAY_F',
            $_addTender: 'ADD_TENDER_F',
            // setAddress2: 'TEMP_ORARY_ADDRESS',
            // setShopList: "SHOP_ORDER_LIST",
            // setVuexAddress:'TEMP_ORARY_ADDRESS'
        }),

        ...mapActions([
            // 'setTab',
            // 'setCategoryTabList',
            // 'setAddress',
            // 'clearAddress',
            // 'setBrowse',
            // 'deleteOne',
            // 'selectCity',
            // 'setSearchHistory',
            // 'clearSearchHistory'
        ])
    },
}
export default vuexData