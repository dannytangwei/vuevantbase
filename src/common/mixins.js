
// import BaseLoading from '../components/base/BaseLoading.vue'
import { mapMutations, mapGetters,mapActions } from 'vuex'

export const pay = {
    methods: {
        getDocNo() {
            var sequence = localStorage.sequence * 1,
                strDateTime = new Date().getTime()
            store = config.user.XF_ORGID,
                till = config.info.Poscode,
                strsequence = '';
            docno = '';
            strsequence = this.padNumber(sequence, 6, 0);
            docno = strDateTime + store + till + strsequence;
            localStorage.sequence = localStorage.sequence * 1 + 1;
            return docno;
        },
        checkoutCash() {
            let me = this;
            try {
                console.log(me.salesTender);
                const CH_CODE = me.$_tenderObj.CHcode;//现金tender
                let cashAmountSum = 0;
                let useTender = [];
                if (CH_CODE) {
                    me.$_salesTender.forEach(element => {
                        if (element.tenderCode == CH_CODE) {
                            cashAmountSum = cashAmountSum + element.payAmount * 1;
                        } else if (element.tenderCode != CHcode) {
                            useTender.push(element);
                        }
                    });
                }
                //如果有多条现金tender，则汇总成一条；再与非现金tender放到一个数组里
                if (cashAmountSum > 0) {
                    useTender.push(
                        {
                            "baseCurrencyCode": "RMB",
                            "tenderCode": "CH",
                            "payAmount": cashAmountSum,
                            "baseAmount": cashAmountSum,
                            "excessAmount": 0,
                            "extendParameter": ''
                        }
                    )
                }
                let request = {
                    transHeader: {
                        ledgerDatetime: me.formatDate(new Date().getTime()),
                        storeCode: me.userInfo.XF_ISSUESTORE,
                        tillId: me.setupInfo.Poscode,
                        docNo: me.getPayDocno()
                    },
                    salesTotal: me.salesTotal,
                    salesItem: [],
                    salesTender: useTender,
                    docKey:me.formatDate(new Date().getTime(),'YYMMDD') + '.' + me.setupInfo.Poscode + '.' + me.getPayDocno()
                };
                //6X服务器规定传参：销售netAmount为商品总金额
                me.$_cartStore.forEach(element => {
                    let item = Object.assign({},element);
                    item.netAmount = (item.qty * item.netAmount).toFixed(2) * 1;
                    request.salesItem.push(item);
                });
                console.log(request);
                
            } catch (error) {
                alert("pay页面-提交销售：" + error);
                this.$toast('我是弹出消息')
            }
            this.$myVue.$emit('loadingEvent', [true,"加载中..."]);
            that.axios.post(that.$_setupInfo.url + "?method=salesTransLiteV61_30", "data=" + JSON.stringify(request))
                .then(function(response) {
                    this.$myVue.$emit('loadingEvent', [false]);
                    console.log(response);
                    let data = response.data;
                    if (data.Header.errcode == 0) {
                    
                    } else {
                        alert(data.Header.errmsg);              
                    }
                })
                .catch(function(error) {
                    this.$myVue.$emit('loadingEvent', [false]);
                    console.log(error);
                });

            //TODO 支付成功后清空cartStore和salesTender

        },
    }
}

export const util = {
    methods:{
        //日期格式转换
        // formatDate(new Date().getTime());//2017-05-12 10:05:44
        // formatDate(new Date().getTime(),'YY年MM月DD日');//2017年05月12日
        // formatDate(new Date().getTime(),'今天是YY/MM/DD hh:mm:ss');//今天是2017/05/12 10:07:45
        formatDate(time, format = 'YY-MM-DD hh:mm:ss') {
            var date = new Date(time);

            var year = date.getFullYear(),
                month = date.getMonth() + 1,//月份是从0开始的
                day = date.getDate(),
                hour = date.getHours(),
                min = date.getMinutes(),
                sec = date.getSeconds();
            var preArr = Array.apply(null, Array(10)).map(function (elem, index) {
                return '0' + index;
            });////开个长度为10的数组 格式为 00 01 02 03

            var newTime = format.replace(/YY/g, year)
                .replace(/MM/g, preArr[month] || month)
                .replace(/DD/g, preArr[day] || day)
                .replace(/hh/g, preArr[hour] || hour)
                .replace(/mm/g, preArr[min] || min)
                .replace(/ss/g, preArr[sec] || sec);

            return newTime;
        },
        /**
         * 获取多少到多少之间的随机整数
         * @param minNum 最小数字
         * @param mmaxNum 最大数字
         * @return 随机数
         */
        getRandomInt(minNum, maxNum) {
            switch (arguments.length) {
                case 1:
                    return parseInt(Math.random() * minNum + 1, 10);
                    break;
                case 2:
                    return parseInt(Math.random() * (maxNum - minNum + 1) + minNum, 10);
                    break;
                default:
                    return 0;
                    break;
            }
        },
        //随机返回一种颜色
        getRandomColor(index) {
            var colorAry = [
                "#16b603",
                "#F2C000",
                "#4BA5E6",
                "#2196F3",
                "#C0D400",
                "#925e8b",
                "#7B4CA7",
                "#F77A36",
                "#e44959",
                "#45B6B4",
                "#03b4d5",
                "#167abc",
                "#D70C6F"
            ];
            var color = "#16b603";
            if (index >= colorAry.length) {
                var randomNum = this.getRandomInt(colorAry.length);
                color = colorAry[randomNum];
            } else {
                color = colorAry[index];
            }
            return color;
        },
         /**
         * @param value 可以是Sting类型或者number类型的数字
         * @return 2位小数的数字,不会四舍五入！
         */
        get2Decimal(value) {
            return parseInt(value*100) / 100;
        }
    }
}

// export const loading = {
//     data() {
//         return {
//             showFlag: true,     //是否显示lodding
//         }
//     },
//     components: {
//         BaseLoading,
//     },
// }

// 返回上一页或者首页
export const goBack = {
    methods: {
        back() {
            window.history.length > 1
                ? this.$router.go(-1)
                : this.$router.push('/home')

        }
    },
}
export const details = {
    methods: {
        ...mapMutations({
            setGoodDetails: 'GOODS_DETAILS',
        }),
        details(val) {
            this.setGoodDetails(val)
            if (this.$route.name === 'Home') {  //加入搜索历史记录
                this.$emit('searchHistory')
            }
            this.$router.push({ path: `/details`, query: { id: val.goodsId || val.id || val.cid} })
        }
    }
}
export const page = {
    data() {
        return {
            dataArr: [],
            total: null,
            loading: false, // 锁
        }
    },

    methods: {
        // newArr, 第二页请求到的数据
        setNewData(newArr) {
            this.dataArr  = this.dataArr.concat(newArr)
        },

        // 起始的记录数
        getCurrentStart() {
            return this.dataArr.length
        },

        // 是否还有更多数据加载
        hasMore() {
            // 说明没有数据要加载了
            console.log(this.dataArr.length);
            console.log(this.total);
            
            if (this.dataArr.length >= this.total) {
                return false
            }
            return true
        },

        // 总条数
        setTotal(total) {
            this.total = total
        },

        // 清空数组
        clearArr() {
            this.setData({
                dataArr: []
            })
            this.total = null
        },

        // 锁的机制
        isLocked() {
            return this.loading ? true : false
        },

        // 加锁
        locked() {
            this.loading = true
        },

        unLocked() {
            this.loading = false
        }
    }
}
