// 公共的js
//日期格式转换
export function FromDates(StringDate) {
    let T = "",
        N = "",
        Y = "",
        R = "";
    for (let i = 0; i < StringDate.length; i++) {
        N = StringDate.substring(0, 4) + "-";
        Y = StringDate.substring(4, 6) + "-";
        R = StringDate.substring(6, 8);
        T = N + Y + R;
        return T;
    }
}
//日期时间去横杠
export function FromDatesCancle(StringDate) {
    let CanTime = [];
    for (let i = 0; i < StringDate.length; i++) {
        if (StringDate[i] !== "-") {
            CanTime.push(StringDate[i]);
        }
    }
    return CanTime.join('');
}


export function test() {
    console.log("我是测试");
    return "docno";
}