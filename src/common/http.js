// 从未使用

import axios from 'axios'

import store from '@/store'
let url = "";
try {
  if(store.getters.setupInfo){
    console.log("http.js文件中获取setupInfo："+JSON.stringify(store.getters.setupInfo));
    url = store.getters.setupInfo.url;
  }
} catch (error) {
  alert("http.js报错："+error)
}

//因为POS项目中，可以在设置界面中修改baseURL，而此文件只会在初始化应用时被加载一次，
// 不知道怎么让url动态响应，所以就不使用这个文件了

//创建axios的一个实例 
var myHttp = axios.create({
  baseURL: url,   
  timeout: 9000,  // 超时
  headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
})

//------------------- 一、请求拦截器 忽略
// instance.interceptors.request.use(function (config) {
//   return config;
// }, function (error) {
//   // 对请求错误做些什么
//   return Promise.reject(error);
// });
//----------------- 二、响应拦截器 忽略
// instance.interceptors.response.use(function (response) {
//   return response.data;
// }, function (error) {
//   // 对响应错误做点什么
//   return Promise.reject(error);
// });
// respone拦截器
// server.interceptors.response.use(
//     response => {
//         const res = response.data
//         if (res.code === ERR_OK) {  // 统一处理
//             return res // 直接返回数据
//         } else {
//             Vue.prototype.$toast(res.msg)
//             return Promise.reject(res)
//         }
//     },
//     error => {
//         Vue.prototype.$toast('网络错误')
//         return Promise.reject(error)
//     }
// )
export default myHttp