import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router) //使用Router插件

// export default new Router({
//   routes: [
//     {
//       path: '/',
//       name: 'home',
//       component: Home
//     },
//     {
//       path: '/about',
//       name: 'about',
//       // route level code-splitting
//       // this generates a separate chunk (about.[hash].js) for this route
//       // which is lazy-loaded when the route is visited.
//       component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
//     }
//   ]
// })

//定义一组路由
const routes = [
  {
    path: '*',
    redirect: '/login'
  },
  {
    name: 'login',
    //这是路由懒加载的
    // component: () => import('./views/login.vue'),//ES6写法
    component: function() {//ES5写法
      return import('./views/login.vue');
    },
    meta: {
      title: '登录'
    }
  },
  {
    name: 'setup',
    // 每个路由应该映射一个组件
    component: () => import('./views/setup.vue'),
    meta: {
      title: '设置'
    }
  },
  {
    name: 'main',
    component: () => import('./views/main.vue'),
    meta: {
      title: '主页'
    }
  },
  {
    name: 'sales',
    component: () => import('./views/sales.vue'),
    meta: {
      title: '销售'
    }
  },
  {
    name: 'pay',
    component: () => import('./views/pay.vue'),
    meta: {
      title: '支付'
    }
  },
  {
    name: 'return',
    component: () => import('./views/return.vue'),
    meta: {
      title: '退货'
    }
  },
  {
    name: 'Home',
    component: () => import('./views/Home.vue'),
    meta: {
      title: 'home'
    }
  },
];

// add route path
routes.forEach(route => {
  route.path = route.path || '/' + (route.name || '');
});

// 创建router实例
const router = new Router({ 
  routes,// (缩写) 相当于 routes: routes
  // linkActiveClass: 'active' 
});

router.beforeEach((to, from, next) => {
  const title = to.meta && to.meta.title;
  if (title) {
    document.title = title;
  }
  next();
});

export {
  router
};