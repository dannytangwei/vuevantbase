import * as types from './mutations-type'

// 改变state
const matutaions = {
    // 使用 ES2015 风格的计算属性命名功能来使用一个常量作为函数名
    [types.SETUPINFO](state,setupInfo) {  //自定义改变state初始值的方法
        state.setupInfo = setupInfo
    },
    [types.USERINFO](state,userInfo) {  
        state.userInfo = userInfo
    },
    //参数goodsArray：是一个数组
    [types.GOODS_ARRAY](state,goodsArray) { 
        state.goodsArray = goodsArray
    },
    [types.PAY_TENDER](state,payTender) { 
        state.payTender = payTender
    },
    //在商品页面加购
    //参数cartStore：是一个对象
    [types.ADD_CART](state,cartStore) { 
        //相当于往数组末尾添加元素，而不是覆盖已有数组
        state.cartStore = [...state.cartStore,cartStore];
    },
    //在购物车页面增加商品数量
    //参数index：表示当前操作的商品
    [types.UPDATE_CART_ADD](state,index) { 
        let cart = state.cartStore;
        let shop = cart[index] || {};
        if(shop.qty){
            shop.qty++
        }
        state.cartStore = [...cart];
    },
    //在购物车页面减少商品数量
    [types.UPDATE_CART_REDUCTION](state,index) { 
        let cart = state.cartStore;
        let shop = cart[index] || {};
        if(shop.qty){
            if(shop.qty-1){
                shop.qty--
            }else{  //当数量为0时，移除此商品
                cart.splice(index,1)
            }
        }
        state.cartStore = [...cart];
    },
    //点击结算按钮时设置pay相关数据
    [types.TO_PAY](state){
        var salesSum =0,
            qtySum = 0;
        state.cartStore.forEach((element,index) => {
            element.salesLineNumber=index;//去到支付页面前给此字段赋值
            salesSum = salesSum+element.netAmount*element.qty;
            qtySum = qtySum+parseInt(element.qty);
        });
        var salesTotal ={
            'cashier':state.userInfo.staffcode,
            // 'vipCode':config.vip?config.vip.xf_vipcode:'',
            'netQty':qtySum,
            'netAmount':salesSum
        };
        state.salesTotal = salesTotal;
        console.log("mutations-TO_PAY中cartStore的值:"+JSON.stringify(state.cartStore));
    },
    [types.ADD_TENDER](state,tender) { 
        state.salesTender = [...state.salesTender,tender];
    },
}

export default matutaions