// 获取状态
// 
//实时监听state值的变化,只有当它的依赖值发生了改变才会被重新计算
export const setupInfo = state => state.setupInfo; 
export const tenderObj = state => state.tenderObj; 
export const userInfo = state => state.userInfo;  
export const goodsArray = state => state.goodsArray;
export const payTender = state => state.payTender;
export const cartStore = state => state.cartStore;
export const salesTotal = state => state.salesTotal;
export const salesTender = state => state.salesTender;