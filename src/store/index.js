import Vue from 'vue'
import Vuex from 'vuex'
import * as actions from './actions'
import * as getters from './getters'
import mutations from './mutations'

Vue.use(Vuex)

// 全局状态
const state = {
  setupInfo:{
    url: 'http://58.249.125.44:31002/API.ashx',    //锦荟测试环境  账号：01000001密码：123456 
    ttpayurl: '',
    mailcode: '',
    mailname: '玉兰广场',
    storecode: '01000001',//锦荟-店铺编号
    storename: '测试店铺',
    Poscode: '01',
    SellerID: '',  //支付商户号
  },
  tenderObj:{
    //锦荟环境
    BKcode:'BK',
    WPcode:'WP', 
    APcode:'AP',
    CHcode:'CH',    //现金
  },
  userInfo: {},           // 用来存储用户信息-登录时获取
  goodsArray:[],         //商品列表-登录时获取
  payTender: {},        // 支付方式-登录时获取
  cartStore:[],         //购物车-存放加购商品数据-在GoodsItems组件中添加数据
  salesTotal:{},        //购物车-加购商品的汇总数据
  salesTender:[],      //销售订单的支付方式数组
}

const debug = process.env.NODE_ENV !== 'production'
export default new Vuex.Store({
  actions,
  getters,
  state,
  mutations,
  strict: debug,
})
