
// s4.用常量来表示方法名
export const USERINFO = 'USERINFO_F'    //自定义方法名
export const SETUPINFO = 'SETUPINFO_F'
export const GOODS_ARRAY = 'GOODS_ARRAY_F'
export const PAY_TENDER = 'PAY_TENDER_F'
export const ADD_CART = 'ADD_CART_F'
export const UPDATE_CART_ADD = 'UPDATE_CART_ADD_F'
export const UPDATE_CART_REDUCTION = 'UPDATE_CART_REDUCTION_F'
export const TO_PAY = 'TO_PAY_F'
export const ADD_TENDER = 'ADD_TENDER_F'
