//生产环境（build）
const production = process.env.NODE_ENV === 'production'

const path = require('path');
function resolve(dir) {
  return path.join(__dirname, dir);
}
module.exports = {
  // build打包输出基本路径
  publicPath: production ? './' : '/',
  // build打包输出基本路径+输出文件目录
  outputDir: 'dist',
  //生产环境的 source map，设置为 false 以加速生产环境构建,会混淆源代码（保护源码）
  //开发环境设置为true，就不会混淆，便于调试
  productionSourceMap: production?false:true,

  //关闭eslint规范
  lintOnSave: false,

  // webpack相关配置，官方文档https://cli.vuejs.org/zh/guide/webpack.html
  configureWebpack: config => {
    // if (production) {  //生产环境
    //   config.devtool = 'source-map' 
    // } else {  // 本地开发环境配置
    //   config.devtool = 'cheap-module-eval-source-map'
    // };
    if (production) {  //生产环境
      
    } else {  // 本地开发环境配置
      config.devtool = 'source-map' //不混淆源码，浏览器调试显示src文件夹，里面有完整源码
    };
    // 开发、生产环境共同配置
    // Object.assign(config, {
      
    // });
  },
  // webpack相关配置
  chainWebpack: config => {
    config.resolve.alias
      .set('@$', resolve('src'))
      .set('assets', resolve('src/assets'))
      .set('components', resolve('src/components'))
      .set('layout', resolve('src/layout'))
      .set('common', resolve('src/common'))
  },
  css: {
    loaderOptions: {
      sass: {
        data: '@import "@/assets/styles/mixin.scss";'
      }
    }
  },


  // ======================================webpack-dev-server 相关配置=====================================
  // 在本地会创建一个虚拟服务端，然后发送请求的数据，并同时接收请求的数据，
  // 这样客户端和服务端进行数据的交互就不会有跨域问题
  devServer: {
    port: 8080, // 端口号
    host: 'localhost',
    https: false,
    open: true, //开发环境build时自动启动浏览器

    //-----------------如果不需要代理跨域，可注释下面代码------------------
    proxy: {
      '/api': {
        // 咔咖文档http://pay.kaka-tech.com/doc/index.php?s=/9&page_id=49
        target: 'http://test.kaka-tech.com/trade/gateway/',  //咔咖测试环境
        ws: true,
        changOrigin: true,  //是否跨域 
        pathRewrite: {
          '^/api': ''
        }
      }
    },
    //------------------------------------------------------------------

    // 开发环境 配置跨域处理,只有一个代理
    //方法1：
    // proxy: 'http://localhost:8080' 
    //方法2：
    // proxy: {
    //   '/api': {
    //     target: process.env.VUE_APP_BASE_API || 'http://127.0.0.1:8080',
    //     changeOrigin: true
    //   }
    // },
    //方法3：
    // proxy: {
    //   '/api': {
    //     target: 'http://58.249.125.44:31002/API.ashx',  // 真实的后台接口地址
    //     ws: true,
    //     changOrigin: true,  //是否跨域 
    //     pathRewrite: {
    //       '^/api': '' //当URL以 '/api/'层级开端时，把 'target'字段对应的值代理成本地IP
    //     }
    //   }
    // },
  

  }
  // ====================================================================================================
};
