# vueVantBase

## Project setup（从git上下载、pull时，如果项目中没有相关依赖，则需要运行下面这条命令）
```
npm install
```

### Compiles and hot-reloads for development（开发环境-运行此条命令，在浏览器生成应用）
```
npm run serve
```

### Compiles and minifies for production（生产环境-打包）
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
